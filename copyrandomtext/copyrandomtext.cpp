// copyrandomtext.cpp : Defines the entry point for the application.
//

#include "stdafx.h"



#include "copyrandomtext.h"

using namespace std;
using namespace Ambiesoft;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	const wchar_t alpha[] = L"abcdefghijklmnopqrstuvwxyz";
	
	random_device rnddev;
	uniform_int_distribution<> randalpha(0, _countof(alpha) - 2);
	
	wstring result;
	int charcount = 5;
	for (int i = 0; i < charcount;++i)
	{
		int rndindex = randalpha(rnddev);
		result += alpha[rndindex];
	}

	if (!SetClipboardText(nullptr, result.c_str()))
	{
		MessageBox(nullptr, I18N(L"Failed to set text on the clipboard."), APPNAME, MB_ICONERROR);
		return -1;
	}

	showballoon(nullptr,
		APPNAME,
		I18N(L"Random text has been set on the clipboard."),
		nullptr,
		5000,
		1,
		FALSE,
		1);
	return 0;
}
